import UIKit

class ViewController: UIViewController {
    private let server = Server()
    private var model: [Deal] = []
    @IBOutlet weak var tableView: UITableView!
    var currentField = SortField.instrument
    var accending = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Deals"
        tableView.register(UINib(nibName: DealCell.reuseIidentifier,
                                 bundle: nil), forCellReuseIdentifier: DealCell.reuseIidentifier)
        tableView.register(UINib(nibName: HeaderCell.reuseIidentifier,
                                 bundle: nil), forHeaderFooterViewReuseIdentifier: HeaderCell.reuseIidentifier)
        tableView.dataSource = self
        tableView.delegate = self
        
        server.subscribeToDeals { deals in
            self.model.append(contentsOf: deals)
            self.sortDeals()
            self.tableView.reloadData()
        }
        print(UIScreen.main.bounds.width)
    }
    
    func sortDeals() {
        switch currentField {
        case .instrument:
            model.sort { left, right in
                if self.accending {
                    return left.instrumentName < right.instrumentName
                } else {
                    return left.instrumentName > right.instrumentName
                }
            }
        case .price:
            model.sort { left, right in
                if self.accending  {
                    return left.price < right.price
                } else {
                    return left.price > right.price
                }
            }
        case .amount:
            model.sort { left, right in
                if self.accending  {
                    return left.amount < right.amount
                } else {
                    return left.amount > right.amount
                }
            }
        case .side:
            model.sort { left, right in
                if self.accending  {
                    return left.side.rawValue < right.side.rawValue
                } else {
                    return left.side.rawValue > right.side.rawValue
                }
            }
        }
    }
}

enum SortField {
    case instrument, price, amount, side
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DealCell.reuseIidentifier, for: indexPath) as! DealCell
        let deal = model[indexPath.row]
        cell.dateLabel.text = self.dateToStr(deal.dateModifier)
        cell.amountLabel.text = String(Int(deal.amount))
        cell.priceLabel.text = String(format: "%.2f", deal.price)
        var instrumentName = deal.instrumentName
        instrumentName.remove(at: String.Index(utf16Offset: 3, in: instrumentName))
        var name = ""
        for _ in 0...5 { name.append(instrumentName.removeFirst()) }
        cell.instrumentNameLabel.text = name
        cell.sideLabel.text = deal.side.rawValue
        switch deal.side {
        case .buy: cell.sideLabel.textColor = .green
        case .sell: cell.sideLabel.textColor = .red
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: HeaderCell.reuseIidentifier) as! HeaderCell

        let switchNameTap = UITapGestureRecognizer(target: self, action: #selector(switchNameSort))
        header.instrumentNameTitlLabel.addGestureRecognizer(switchNameTap)
        header.instrumentNameTitlLabel.isUserInteractionEnabled = true
        let switchPriceTap = UITapGestureRecognizer(target: self, action: #selector(switchPriceSort))
        header.priceTitleLabel.addGestureRecognizer(switchPriceTap)
        header.priceTitleLabel.isUserInteractionEnabled = true
        let switchAmountTap = UITapGestureRecognizer(target: self, action: #selector(switchAmountSort))
        header.amountTitleLabel.addGestureRecognizer(switchAmountTap)
        header.amountTitleLabel.isUserInteractionEnabled = true
        let switchSideTap = UITapGestureRecognizer(target: self, action: #selector(switchSideSort))
        header.sideTitleLabel.addGestureRecognizer(switchSideTap)
        header.sideTitleLabel.isUserInteractionEnabled = true
        
        return header
    }
    
    @objc func switchNameSort() {
        if currentField == .instrument {
            self.accending.toggle()
        } else {
            self.accending = true
        }
        currentField = .instrument
    }
    
    @objc func switchPriceSort() {
        if currentField == .price {
            self.accending.toggle()
        } else {
            self.accending = true
        }
        currentField = .price
    }
    
    @objc func switchAmountSort() {
        if currentField == .amount {
            self.accending.toggle()
        } else {
            self.accending = true
        }
        currentField = .amount
    }
    
    @objc func switchSideSort() {
        if currentField == .side {
            self.accending.toggle()
        } else {
            self.accending = true
        }
        currentField = .side
    }
    
    //  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //    return 60
    //  }
    
    private func dateToStr(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm:ss dd.MM.yyyy"
        let str = formatter.string(from: date)
        return str
    }
}

